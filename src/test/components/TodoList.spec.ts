import { describe, it, expect } from 'vitest';
import { mount } from '@vue/test-utils';
import { Todo } from '~/shared/types';
import TodoList from '~/components/TodoList.vue';

const todos: Todo[] = [
  {
    id: 1,
    title: 'todo test 1',
    completed: false,
  },
  {
    id: 2,
    title: 'todo test 2',
    completed: true,
  },
];

describe('Todo List', () => {
  it('should render component', () => {
    const wrapper = mount(TodoList, {
      props: {
        todos,
      },
    });

    expect(wrapper.html()).toBeDefined();
    expect(
      wrapper.get('[data-testid]=todo-list-container').element
        .childElementCount,
    ).toBe(todos.length);
  });
});
