import { describe, it, expect, vi, afterEach } from 'vitest';
import { shallowMount } from '@vue/test-utils';
import { Todo } from '~/shared/types';
import TodoForm from '~/components/TodoForm.vue';

describe('Todo Form', () => {
  afterEach(() => {
    vi.restoreAllMocks();
  });

  it('should render component', () => {
    const wrapper = shallowMount(TodoForm);

    expect(wrapper.html()).toBeDefined();
  });

  it('emit add-todo on submit', () => {
    const wrapper = shallowMount(TodoForm);

    const form = wrapper.find('form');

    const todo: Omit<Todo, 'id'> = {
      title: 'todo test 1',
      completed: false,
    };

    form.find('input[name="title"]').setValue(todo.title);
    form.find('input[name="completed"]').setValue(todo.completed);

    form.trigger('submit');

    expect(wrapper.emitted()).toHaveProperty('addTodo');
    expect(wrapper.emitted().addTodo[0]).toEqual([todo]);
  });
});
