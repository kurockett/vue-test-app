import { describe, it, expect } from 'vitest';
import { mount } from '@vue/test-utils';
import { Todo } from '~/shared/types';
import TodoItem from '~/components/TodoItem.vue';

const todos: Todo[] = [
  {
    id: 1,
    title: 'todo test 1',
    completed: false,
  },
  {
    id: 2,
    title: 'todo test 2',
    completed: true,
  },
];

describe('Todo Item', () => {
  it('should render component', () => {
    const wrapper = mount(TodoItem, {
      props: {
        todo: todos[0],
      },
    });

    expect(wrapper.html()).toBeDefined();
  });

  it.each`
    todo        | containedClasses
    ${todos[0]} | ${['']}
    ${todos[1]} | ${['line-through', 'text-base']}
  `(
    'should add line-through class if completed $todo.completed',
    ({ todo, containedClasses }) => {
      const wrapper = mount(TodoItem, {
        props: {
          todo,
        },
      });
      const classes = wrapper
        .get('[data-testid]=todo-item')
        .find('p')
        .classes();

      expect(
        containedClasses.every((containedClass: string) =>
          classes.includes(containedClass),
        ),
      ).toBe(todo.completed);
    },
  );
});
